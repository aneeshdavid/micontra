app.controller("userCtrl", function ($scope, HttpService) {

  $scope.addUser = function () {
    var req = {
      method: 'POST',
      url: 'http://localhost:3000/api/v1/user/add',
      data: JSON.stringify({
        firstName: $scope.firstName,
        lastName: $scope.lastName,
        userName: $scope.userName,
        password: $scope.password,
      }),
      callBack: function (res) {
        var errors = res.result.errors;
        if (errors) {
          for (var error in errors) {
            $scope.form.userName.$setValidity(error, errors[error]);
          }
        }
      }
    }
    HttpService.sendRequest(req);
  }
  $scope.validateUser = function () {
    var req = {
      method: 'POST',
      url: 'http://localhost:3000/api/v1/user/isDuplicateUser',
      data: JSON.stringify({
        userName: $scope.userName
      }),
      callBack: function (res) {
        $scope.form.userName.$setValidity("isNotExist", res.isNotExist);
      }
    }
    HttpService.sendRequest(req);
  }

});