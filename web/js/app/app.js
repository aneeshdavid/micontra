var app;
(function () {
  app = angular.module('myApp', ['ui.router']);

  app.run(function ($rootScope, $location, $state, LoginService) {
    $rootScope.$on('$stateChangeStart',
      function (event, toState) {
        console.log('Changed state to: ' + toState);
      });

    if (!LoginService.isAuthenticated()) {
      $state.transitionTo('login');
    }
  });

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'login.html',
        controller: 'LoginController'
      }).state('home', {
        url: '/home',
        templateUrl: 'home.html',
        controller: 'HomeController'
      }).state('register', {
        url: '/user',
        templateUrl: '../web/views/registerUser.html',
        controller: 'userCtrl'
      });
  }]);

  app.controller('LoginController', function ($scope, $rootScope, $stateParams, $state, LoginService) {
    $rootScope.title = "Application Login";

    $scope.formSubmit = function () {
      var userObj = {
        username: $scope.username,
        password: $scope.password,
        callBack: function (callBackObj) {
          if (callBackObj.login == true) {
            $scope.error = '';
            $scope.username = '';
            $scope.password = '';
            $state.transitionTo('home');
          } else {
            $scope.error = "Incorrect username/password !";
          }
        }
      }
      LoginService.login(userObj);
    };

  });

  app.controller('HomeController', function ($scope, $rootScope) {
    $rootScope.title = "AngularJS Login Sample";

  });

  app.factory('LoginService', function ($http) {
    var authenticated = false;
    return {
      login: function (userObj) {

        var dataObj = {
          username: userObj.username,
          password: userObj.password
        }

        var req = {
          method: 'POST',
          url: 'http://localhost:3000/api/v1/user/login?api-key=123',
          data: { "username": "" + userObj.username + "", "password": "" + userObj.password + "" }
        }
        $http(req).success(function (data) {
          userObj.callBack(data)
        })
          .error(function () {
            return false;
          });
        return this.isAuthenticated;
      },
      isAuthenticated: function () {
        return authenticated;
      }
    };

  });

  app.factory('HttpService', function ($http) {
    return {
      sendRequest: function (req) {
        req.url += adppendApiKey(req.url);
        $http(req).success(function (data) {
          if (req.callBack) {
            req.callBack(data)
          }
          return true;
        })
          .error(function (err) {
            console.log(err);
            return false;
          });
      }
    };

  });
  var adppendApiKey = function (url) {
    var kvp = url.split('?');
    var newKvp = '?';
    if (kvp.length > 1) {
      newKvp = '&'
    }
    return newKvp + 'time=' + new Date().getTime() + '&api-key=123'
  }
})();