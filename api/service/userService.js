var userDAO = require('../dao/userDAO');
var cryptoUtil = require('../util/cryptoUtil');
exports.doLogin = async function (userName, password) {
  var loginStatus = false;
  if (userName == "" || password == "") {
    return login;
  }
  /*var userObj = {
    userName: userName,
    password: cryptoUtil.encrypt(password)
  }*/
  var userArr = await userDAO.findByUserName(userName)
  var loginStatus = false;
  if (userArr.length > 0) {
    var user = userArr[0];
    if (cryptoUtil.decrypt(user.password) == password) {
      loginStatus = true;
    }
  }
  userDAO.addLoginEntry(userName, loginStatus);
  return loginStatus;
}
exports.add = async function (userObj) {
  var result = {};
  var validation = await validate(userObj);
  if (!validation.isValid) {
    result.errors = validation
    return result;
  } else {
    userObj.password = cryptoUtil.encrypt(userObj.password);
    result = await userDAO.save(userObj)
    return result;
  }

}
exports.isValidUser = async function (userName) {
  var isExist = await userDAO.isUserExist(userName);
  return !isExist
}
var validate = async function (userObj) {
  var validationObj = {}
  var isValid = true;
  /**userName */
  var userName = userObj.userName.$modelValue;
  var isExist = await userDAO.isUserExist(userName);
  validationObj.isNotExist = !isExist;
  if (isExist) {
    isValid = false;
  }
  /****/
  validationObj.isValid = isValid;
  return validationObj;
}