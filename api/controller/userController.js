var express = require('express');
var user_route = express.Router();

var userService = require('../service/userService');

user_route.post("/isDuplicateUser", async function (req, res) {
  var result = await userService.isValidUser(req.body.userName);
  var resObj = {};
  resObj.isNotExist = result;
  res.end(JSON.stringify(resObj));
});
user_route.post("/add", async function (req, res) {
  var result = await userService.add(req.body)
  var resObj = {};
  resObj.result = result;
  res.end(JSON.stringify(resObj));
});
user_route.post("/login", async function (req, res) {
  var user_name = req.body.username;
  var password = req.body.password;
  var login = await userService.doLogin(user_name, password)
  var resObj = {};
  resObj.login = login;
  res.end(JSON.stringify(resObj));
});
module.exports.router = user_route;