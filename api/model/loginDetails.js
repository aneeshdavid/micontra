const mongoose = require('mongoose');
const Schema = mongoose.Schema;

loginSchema = new Schema({
  userName: {
    type: String,
    require: true
  },
  loginStatus: {
    type: Boolean,
    require: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});
module.exports = mongoose.model('login_details', loginSchema);