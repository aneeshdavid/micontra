const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  userName: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true
  },
 /* sex: {
    type: String,
    require: true
  },
  age: {
    type: Number,
    require: true
  },*/
  firstName: {
    type: String,
    require: true
  },
  lastName: {
    type: String,
    require: true
  }
}, {
    timestamps: true
  });

module.exports = mongoose.model('user', userSchema);