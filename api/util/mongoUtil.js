var mongoose = require('mongoose');

var _db;
module.exports = {
  connectToServer: function () {
    if (_db) {
      return _db;
    }
    mongoose.Promise = global.Promise;
    var uri = "mongodb+srv://app-contra-rw:$welcome1@aneeshdavid-6oqt0.mongodb.net/contra_db?retryWrites=true"
    _db = mongoose.connect(uri, {
      useNewUrlParser: true
    }).then(() => {
      console.log("Successfully connected to the database");
    }).catch(err => {
      console.log('Could not connect to the database. Exiting now...', err);
      process.exit();
    });
    return _db;
  },

  getDb: function () {
    return _db;
  }
};

