const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require('cors')
const session = require('express-session')
const uuid = require('uuid/v4')

app.use(cors());
app.use(bodyParser.json());
/****************************DATABASE********************************/
const mongoUtil = require('./util/mongoUtil');
mongoUtil.connectToServer();
var db = mongoUtil.getDb();
mongoUtil.connectToServer();
/****************************SESSION*********************************/
app.use(session({
  genid: (req) => {
    console.log('Inside the session middleware')
    return uuid() // use UUIDs for session IDs
  },
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}))
/****************************FILTER**********************************/
var apiKeys = ['123'];
app.use("", function (req, res, next) {
  var key = req.query['api-key'];
  if (!key) {
    return next(error(400, 'api key required'));
  }
  if (!~apiKeys.indexOf(key)) {
    return next(error(401, 'invalid api key'));
  }
  console.log(req.sessionID)
  next();
});
/****************************ROUTERS*********************************/
app.use('/api/v1', require('./routes').router);
/****************************GLOBAL ERRORS***************************/
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send({ error: err.message });
});
app.use(function (req, res) {
  res.status(404);
  res.send({ error: "Not found" });
});
/******************************SERVER********************************/
app.listen(3000, function () {
  console.log("Started on PORT 3000");
})