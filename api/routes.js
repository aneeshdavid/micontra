var express = require('express');
var router = express.Router();


router.use('/user', require('./controller/userController').router);
module.exports.router = router;